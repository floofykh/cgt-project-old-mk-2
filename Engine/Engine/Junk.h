#ifndef UTILITIES_H
#define UTILITIES_H

	//=========================================================================
	// Macro Defines
	//=========================================================================

	#if ASSERT
		#if WIN32
			#define Assert(expression) if (!(expression)) { DebugBreak(); }
		#else
			#define Assert(expression) if (!(expression)) { *(int*)NULL=1; }
		#endif
	#else
		#define Assert(expression)
	#endif

	#define Breakpoint Assert(1==0)

	#define uint64 	uint64_t
	#define uint32 	uint32_t
	#define uint16 	uint16_t
	#define uint8  	uint8_t

	#define int64 	int64_t
	#define int32 	int32_t
	#define int16 	int16_t
	#define int8  	int8_t

	#define float32 float
	#define float64 double

	#define schar 	signed char
	#define uchar 	unsigned char
	
	#define Bytes(value) 		((value) * 8LL))
	#define Kilobytes(value) 	((value) * (1024LL))
	#define Megabytes(value) 	(Kilobytes(value) * (1024LL))
	#define Gigabytes(value) 	(Megabytes(value) * (1024LL))
	#define Terabytes(value) 	(Gigabytes(value) * (1024LL))
	#define Pentabytes(value) 	(Terabytes(value) * (1024LL))
	
	#define ArrayCount(array) ((sizeof(array)) / (sizeof((array)[0])))

	#define Square(x) ((x)*(x))
	#define Max(a, b) ((a)>(b)?(a):(b))
	#define Min(a, b) ((a)<(b)?(a):(b))

	#define RoundFloatToInt(number) (int32)((number) + 0.5f)

	#define Pi32 				(3.14159265359f)
	#define Pi64 				(3.141592653589793238462643383279502884L)

	#define Deg2Rad32		(Pi32/180.0f)
	#define Deg2Rad64		(Pi64/180.0)

	#define Rad2Deg32		(180.0f/Pi32)
	#define Rad2Deg64		(180.0/Pi64)

	#define internal_function 	static
	#define local_persist 		static
	#define global_variable 	static
	#define shared_method		static

	#define Loop(x) 				for(int32 index=0; index<(x); index++)
	#define ULoop(x) 				for(uint32 index=0; index<(x); index++)
	#define RLoop(x)				for(int32 index=(x)-1; index>0; index--)
	#define RULoop(x)				for(uint32 index=(x)-1; index>0; index--)

	#define Foreach(list, index) 	for(int32 (index)=0; (index)<(list); (index)++)
	#define UForeach(list, index) 	for(uint32 (index)=0; (index)<(list); (index)++)
	#define RForeach(list, index) 	for(int32 (index)=(list)-1; (index)>=0; (index)--)
	// TODO(Jonny): This will will go infinitely because an unsigned integer WILL ALWAYS
	//				be Greater-Than or Equal-To Zero... 
	#define RUForeach(list, index) 	for(uint32 (index)=(list)-1; (index)>=0; (index)--)

	#define WINDOW_WIDTH 	(1920)
	#define WINDOW_HEIGHT 	(1080)

	#define SCENE_SCALE 	(0.08f)

	#if ALLOW_OUTPUT
		#include <iostream>

		#define Output(expression) 		std::cout << expression << std::flush // lol toilet
		#define OutputLn(expression)	std::cout << expression << std::endl
	#else
		#define Output(espression)
		#define OutputLn(expression)
	#endif

	// Printf Formats:
	//		%d 		int
	//		%x		hex
	//		%ld		long
	//		%u 		unsigned
	//		%lu 	unsigned long
	//		%c 		single char
	//		%s 		char *
	//		%f 		float (entire number)
	//		%g 		float (important part)
	//		%lf 	double (entire number)
	//		%lg 	double (important part)
	#if ALLOW_OUTPUT
		#include <stdio.h>

		#define Printf(...) printf(__VA_ARGS__); fflush(stdout)
	#else
		#define Printf(...)
	#endif

	inline uint32
	SafeTruncateSize64(uint64 value)
	{
		Assert(value <= 0xFFFFFFFF);
		uint32 result = (uint32)value;
		return result;
	}

	inline float32
	RemoveDecimalFromFloat(float32 number)
	{
		float32 result = (float32)((int32)number);
		return result;
	}

	internal_function uint32
	StringLength(char *text)
	{
		uint32 count = 0;
		while(*text++)
		{
			count++;
		}
		return count;
	}

	// NOTE(Jonny): Isn't very accurate, but works fast!
	internal_function float32
	FastSquareRoot(float32 number)
	{
		uint32 result = *(uint32 *) &number;
		result += 127 << 23;
		result >>= 1;
		return *(float32*) &result;
	}

	// NOTE(Jonny): Allocates Memory. called free() after use
	internal_function char *
	ConcatonateStrings(char *string1, char *string2)
	{
		uint32 string1Length = StringLength(string1);
		uint32 string2Length = StringLength(string2);

		char *result = (char *)malloc(8 * (string1Length + string2Length));
		Assert(result != NULL);

		ULoop(string1Length)
		{
			result[index] = string1[index];
		}
		ULoop(string2Length)
		{
			result[string1Length+index] = string2[index];
		}

		result[string1Length+string2Length] = '\0';

		return result;
	}

	internal_function char *
	ConcatonateStringAndInt(char *string, int32 integer)
	{
		char *result;
		char integerAsChar[10];
		#if WIN32
			sprintf_s(integerAsChar, sizeof(char) * 10, "%d", integer);
		#else
			sprintf(integerAsChar, "%d", integer);
		#endif
		result = ConcatonateStrings(string, integerAsChar);
		return result;
	}
	
	internal_function char *
	ConcatonateIntAndString(int32 integer, char *string)
	{
		char *result;
		char integerAsChar[10];
		#if WIN32
			sprintf_s(integerAsChar, sizeof(char) * 10, "%d", integer);
		#else
			sprintf(integerAsChar, "%d", integer);
		#endif
		result = ConcatonateStrings(integerAsChar, string);
		return result;
	}

#endif