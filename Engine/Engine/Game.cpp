#include <GL\glew.h>
#include <GL\glew.h>

#define GLM_FORCE_RADIANS
#include <glm\glm.hpp>
#include <glm\gtc\matrix_transform.hpp>

#include <assimp\Importer.hpp>
#include <assimp\scene.h>
#include <assimp\postprocess.h>

#include <tinyxml2.h>
#include <iostream>
#include <math.h>

#include "OpenGLHelper.h"
#include "Game.h"
#include "Input.h"
#include "Junk.h"
#include "GetLetterPosition.cpp"

static HSAMPLE
loadSample(char * filename) //Load Sample into the game
{
	HSAMPLE music;
	music = BASS_SampleLoad(FALSE, filename, 0, 0, 3, BASS_SAMPLE_LOOP);
	return music;
}

static void
createGameMusic(HSAMPLE sounds[]) //Creates Music
{
	if(!BASS_Init(-1, 44100, 0, 0, NULL))
	{
		std::cout << "Cant't initialise sound." << std::endl;
	}
	sounds[0] = loadSample("Sounds//Keep_The_Pace.mp3");
	sounds[1] = loadSample("Sounds//Metal_Race.mp3");
	
#if 0
	sounds[0] = loadSample("Sounds//Metal_Race.mp3"); //Game
	sounds[1] = loadSample("Sounds//Switch_It_Up.mp3"); //Game
	sounds[2] = loadSample("Sounds//Keep_The_Pace.mp3"); //Game
	sounds[3] = loadSample("Sounds//Morning_Stroll.mp3"); //Menu
	sounds[4] = loadSample("Sounds//Funeral_March_by_Chopin.mp3"); //Game Over
	sounds[5] = loadSample("Sounds//You_Make_Me_Feel_Good.mp3"); //Level Complete
	sounds[6] = loadSample("Sounds//Crowd_Applause.wav"); //Sound Effect

	std::cout << "Press P to pause music and R to resume music" << std::endl;
#endif
}

static void
deleteMusic() //Deleted music to free up memory
{
	BASS_Stop();
	BASS_Free();
}



static void
playMusic(HSAMPLE sound) //Plays Music: 0 - Game, 1 - Highscore, 2 - LevelComplete, 3 - Mainmenu
{
	HCHANNEL song = BASS_SampleGetChannel(sound, FALSE);
	BASS_ChannelSetAttributes(song, -1 , 50, (rand()%201)-100);
	BASS_ChannelPlay(song, TRUE);

	if (!BASS_ChannelPlay(song, FALSE))
	{
		std::cout << "Can't play sample" << std::endl;
	}
}

#if COLLISION_BOXES
	Mesh AABB::wireframeMesh;
#endif

static void
initSkybox(GameState *gameState)
{
	gameState->shaders.skybox = initShaders("Shaders/skyboxCubemap.vert", "Shaders/skyboxCubemap.frag");
	gameState->textures.skyboxCubemap = loadCubemap("Textures/nec_city",
	                                                "city_bk.tga",
	                                                "city_ft.tga",
	                                                "city_rt.tga",
	                                                "city_lf.tga",
	                                                "city_up.tga",
	                                                "cityp_dn.tga");
	gameState->mesh.cube = loadMesh("Models/cube.3ds"/*, true, glm::vec3(1.0f)*/);
}

static Building
createBuilding(glm::vec3 pos, glm::vec3 scale, AABB defaultBody, AABB defaultRoof)
{
	Building result = {};

	result.pos = pos;
	result.scale = scale;
	result.rotation = glm::vec3(0.0f, 0.0f, 0.0f);

	result.aabb_roof = defaultRoof;
	result.aabb_roof.position += pos;
	result.aabb_body = defaultBody;
	result.aabb_body.position += pos;
	result.aabb_body.size = result.aabb_body.size * scale;

	result.isInitialized = true;

	float offset = 5.0f;
	result.aabb_roof.position.y -= offset;
	result.aabb_body.position.y -= offset;

	return result;
}

void LoadLevel(const char *file, GameState *gameState)
{
	//Open file and print error and return if there was an error.
	tinyxml2::XMLDocument level;
	if (level.LoadFile(file) != tinyxml2::XML_NO_ERROR)
	{
		std::cout << "Failed to load level at " << file << std::endl << "Error: " << level.ErrorName() << std::endl;
		return;
	}
	level.Print();

	//Get the element holding all the models used
	tinyxml2::XMLElement *models = level.FirstChildElement();
	models = models->FirstChildElement();
	//Get the first child element and get its name and file path
	tinyxml2::XMLElement *building = models->FirstChildElement();
	//Get the number of models and loop through them
	int count = models->IntAttribute("Count");
	for (int i = 0; i < count; i++)
	{
		//Get its name and file path
		const char *name = building->Attribute("Name");
		const char *path = building->Attribute("Path");
		//Then get the size of it
		tinyxml2::XMLElement *positionElement = building->FirstChildElement();
		float x = positionElement->FloatAttribute("x");
		float y = positionElement->FloatAttribute("y");
		float z = positionElement->FloatAttribute("z");
		glm::vec3 position = glm::vec3(x, y, z);
		//Then get the size of it
		tinyxml2::XMLElement *sizeElement = positionElement->NextSiblingElement();
		x = sizeElement->FloatAttribute("x");
		y = sizeElement->FloatAttribute("y");
		z = sizeElement->FloatAttribute("z");
		glm::vec3 size = glm::vec3(x, y, z);

		//Load in the model and create its bounding boxes. 
		Mesh newMesh = loadMesh(path);
		AABB body = AABB();
		AABB roof = AABB();
		body.size = size;
		body.position = position;

		roof.size = body.size;
		roof.size.y = 15.0f;
		roof.position = body.position;
		roof.position.y = roof.size.y / 2.0f;
		
		newMesh.bodyAABB = body;
		newMesh.roofAABB = roof;
		gameState->mesh.buildings[i] = newMesh;


		//Move to next element
		if (i != count - 1)
			building = building->NextSiblingElement();
	}

	//Get the element holding all game objects
	tinyxml2::XMLElement *objects = models->NextSiblingElement();
	building = objects->FirstChildElement();
	//Get the number of objects and loop through them
	count = objects->IntAttribute("Count");
	for (int i = 0; i < count; i++)
	{
		const char *name = building->Attribute("Name");
		bool startBuilding = building->BoolAttribute("StartBuilding");
		//Get the buildings position
		tinyxml2::XMLElement *posElement = building->FirstChildElement();
		float x = posElement->FloatAttribute("x");
		float y = posElement->FloatAttribute("y");
		float z = posElement->FloatAttribute("z");
		glm::vec3 position = glm::vec3(x, y, z);
		//Size
		tinyxml2::XMLElement *sizeElement = posElement->NextSiblingElement();
		x = sizeElement->FloatAttribute("x");
		y = sizeElement->FloatAttribute("y");
		z = sizeElement->FloatAttribute("z");
		glm::vec3 modelSize = gameState->mesh.buildings[0].bodyAABB.size;
		glm::vec3 size = glm::vec3(x, y, z) / modelSize;

		gameState->building[i] = createBuilding(position, size, gameState->mesh.buildings[0].bodyAABB, gameState->mesh.buildings[0].roofAABB);

		if (startBuilding)
		{
			gameState->startPosition = position;
			gameState->startPosition.y += modelSize.y / 2.0f + 100.0f;
		}

		if (i != count - 1)
			building = building->NextSiblingElement();
	}
}

#if COLLISION_BOXES
	static void
	initCollisionBoxMesh()
	{
		GLfloat vertices[] = {	
			-0.5f, 0.5f, -0.5f, //Top back left
			0.5f, 0.5f, -0.5f, //Top back right
			-0.5f, -0.5f, -0.5f, //Bottom back left
			0.5f, -0.5f, -0.5f, //Bottom back right
			-0.5f, 0.5f, 0.5f, //Top front left
			0.5f, 0.5f, 0.5f, //Top front right
			-0.5f, -0.5f, 0.5f, //Bottom front left
			0.5f, -0.5f, 0.5f, //Bottom front right
		};
		GLuint indices[] = {
			0, 1, 0, 2, 0, 4,
			1, 3, 1, 5,
			2, 3, 2, 6,
			3, 7,
			4, 5, 4, 6,
			5, 7,
			6, 7
		};

		AABB::wireframeMesh.vaos = new GLuint[1];
		AABB::wireframeMesh.numMeshes = 1;
		AABB::wireframeMesh.numOfIndices = new GLuint[1];
		AABB::wireframeMesh.numOfIndices[0] = 24;
		GLuint VBOs[2];
		glGenVertexArrays(1, AABB::wireframeMesh.vaos);
		glGenBuffers(2, VBOs);
		glBindVertexArray(AABB::wireframeMesh.vaos[0]);

		//Send Vertices
		glBindBuffer(GL_ARRAY_BUFFER, VBOs[0]);
		glBufferData(GL_ARRAY_BUFFER, 8 * 3 * sizeof(GLfloat), vertices, GL_STATIC_DRAW);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
		glEnableVertexAttribArray(0);
		//Send Indices
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, VBOs[1]);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, 24 * sizeof(GLuint), indices, GL_STATIC_DRAW);

		glBindVertexArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

	static void
	drawCollisionBox(AABB collisionBox, GLuint wireframeShader, glm::mat4 projMat, glm::mat4 viewMat)
	{
		glDisable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glUseProgram(wireframeShader);
		glBindVertexArray(AABB::wireframeMesh.vaos[0]);
		int colourLoc = glGetUniformLocation(wireframeShader, "colour");
		if (colourLoc != -1)
		{
			glUniform3f(colourLoc, 1.0f, 0.0f, 0.0f);
		}
		int modelViewLoc = glGetUniformLocation(wireframeShader, "modelViewMat");
		int projectionLoc = glGetUniformLocation(wireframeShader, "projectionMat");

		glm::mat4 modelMatrix = glm::mat4(1.0f);
		modelMatrix = glm::translate(modelMatrix, collisionBox.position);
		modelMatrix = glm::scale(modelMatrix, collisionBox.size);
		glm::mat4 modelViewMatrix = viewMat * modelMatrix;

		setUniform(modelViewLoc, modelViewMatrix);
		setUniform(projectionLoc, projMat);

		glDrawElements(GL_LINES, AABB::wireframeMesh.numOfIndices[0], GL_UNSIGNED_INT, 0);
		glBindVertexArray(0);
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
	}
#endif

#if 0
static void
initBuildings(GameState *gameState)
{
	int numBuildings = 0;

	//Goal building
	gameState->building[numBuildings++] = createBuilding(glm::vec3(2500.0f, 200.0f, 2500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	//All other buildings
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 0.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(500.0f, 50.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 50.0f, 500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(500.0f, 50.0f, 500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	gameState->building[numBuildings++] = createBuilding(glm::vec3(1000.0f, 100.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 100.0f, 1000.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(1000.0f, 100.0f, 1000.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	gameState->building[numBuildings++] = createBuilding(glm::vec3(1500.0f, 100.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 100.0f, 1500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(1500.0f, 100.0f, 1500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	gameState->building[numBuildings++] = createBuilding(glm::vec3(2000.0f, 150.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 150.0f, 2000.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(2000.0f, 150.0f, 2000.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	gameState->building[numBuildings++] = createBuilding(glm::vec3(2500.0f, 200.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 200.0f, 2500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(2500.0f, 200.0f, 2500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	gameState->building[numBuildings++] = createBuilding(glm::vec3(3000.0f, 150.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 150.0f, 3000.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(3000.0f, 150.0f, 3000.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);

	gameState->building[numBuildings++] = createBuilding(glm::vec3(3500.0f, 170.0f, 0.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(0.0f, 170.0f, 3500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
	gameState->building[numBuildings++] = createBuilding(glm::vec3(3500.0f, 170.0f, 3500.0f),
	                                                     glm::vec3(1.0f, 1.0f, 1.0f),
	                                                     gameState->baseBodyAABB[0],
	                                                     gameState->baseRoofAABB[0]);
}
#endif

static HighScoreTable
BubblesortHighScore(HighScoreTable highScoreTable)
{
	HighScoreTable result = highScoreTable;

	for (int i = 0; i<NumberOfHighScores; i++)
	{
		for (int k = NumberOfHighScores - 1; k >= 0; k--)
		{
			if (k != NumberOfHighScores - 1)
			{
				if (result.score[k] < result.score[k + 1])
				{
					int swapScore = result.score[k];
					result.score[k] = result.score[k + 1];
					result.score[k + 1] = swapScore;

					char *swapName = result.name[k];
					result.name[k] = result.name[k + 1];
					result.name[k + 1] = swapName;
				}
			}
		}
	}

	return result;
}

void
initGame(GameState *gameState)
{
	//TEST FOR MUSIC
	createGameMusic(gameState->sounds);
	playMusic(gameState->sounds[0]); //Music for game

	gameState->shaders.main = initShaders("Shaders/blank.vert", "Shaders/blank.frag");
	#if COLLISION_BOXES
		gameState->shaders.wireFrame = initShaders("Shaders/Wireframe.vert", "Shaders/Wireframe.frag");
		initCollisionBoxMesh();
	#endif

	gameState->mesh.enemy = loadMesh("Models/obeseMale/obese_male.obj");
	/*gameState->mesh.buildings[0] = loadMesh("Models/Buildings/Building1.FBX",
	                                        true,
	                                        gameState->baseBodyAABB, gameState->baseRoofAABB);*/

	gameState->textures.building = loadImage("Textures/Bricks2_FIX.png", 4);
	
	gameState->shaders.hud = initShaders("Shaders/hud.vert", "Shaders/hud.frag");
	gameState->textures.textSheet = loadImage("Textures/freemono.png", 4,
	                                          gameState->textSheetWidth,
	                                          gameState->textSheetHeight);
	generate2DMesh(gameState->mesh.hud.VBOarray,
	               gameState->mesh.hud.mesh,
	               (float)gameState->textSheetWidth,
	               (float)gameState->textSheetHeight,
				   16.0f, 16.0f);
	glUseProgram(gameState->shaders.hud);
	GLuint transMat = glGetUniformLocation(gameState->shaders.hud, "u_trans");
	glm::mat4 empty = {};
	setUniform(transMat, empty);


	initSkybox(gameState);
	//initBuildings(gameState);
	LoadLevel("Models/Buildings/test.pza", gameState);

	gameState->camera = {};
	//gameState->camera.pos = glm::vec3(0.0f, 40.0f, 0.0f);
	gameState->camera.pos = gameState->startPosition;
	gameState->camera.startPos = gameState->camera.pos;
	gameState->camera.lookAt = glm::vec3(0.0f, 0.0f, -1.0f);
	gameState->camera.aabb.position = gameState->camera.pos;
	gameState->camera.aabb.size = glm::vec3(10.0f, 40.0f, 10.0f);
	gameState->camera.currentBuilding = -1;
	gameState->camera.score = 42;
	gameState->camera.numberOfPizzaSlices = 6;


	gameState->enemy[0] = {};
	gameState->enemy[0].pos = glm::vec3(400.0f, 120.0f, 0.0f);
	float scale = 4.0;
	gameState->enemy[0].scale = glm::vec3(scale, scale, scale);
	gameState->enemy[0].aabb.position = gameState->enemy[0].pos;
	gameState->enemy[0].aabb.size = gameState->enemy[0].scale * 0.01f;
	gameState->enemy[0].isInitialized = true;

	gameState->mainMenu.isActive = true;
	uint32 temp;
	gameState->textures.mainMenuStart 		= loadImage("Textures/start.png", 4, temp, temp);
	gameState->textures.mainMenuHighScore 	= loadImage("Textures/highscore.png", 4, temp, temp);
	gameState->textures.mainMenuHelp 		= loadImage("Textures/help.png", 4, temp, temp);
	gameState->textures.mainMenuQuit 		= loadImage("Textures/quit.png", 4, temp, temp);

	gameState->textures.mainMenuTitle 		= loadImage("Textures/title.png", 4, temp, temp);
	gameState->textures.mainMenuBackground 	= loadImage("Textures/main_menu_background.jpg", 4, temp, temp);

	gameState->textures.helpScreen = loadImage("Textures/HELP_SCREEN.png", 4, temp, temp); //HELP OVERLAY


	generate2DMesh(gameState->mainMenu.gl.VBOarray,
	               gameState->mainMenu.gl.mesh,
	               1.0f, 1.0f,
	               2.0f, 1.0f);
	generate2DMesh(gameState->mainMenu.background_gl.VBOarray,
	               gameState->mainMenu.background_gl.mesh,
	               1.0f, 1.0f,
	               1.0f, 1.0f);


	gameState->mainMenu.highScoreTable.name[0] = "Sixty-Nine";
	gameState->mainMenu.highScoreTable.name[1] = "Sean";
	gameState->mainMenu.highScoreTable.name[2] = "Gary";
	gameState->mainMenu.highScoreTable.name[3] = "Christie";
	gameState->mainMenu.highScoreTable.name[4] = "Pedo";
	gameState->mainMenu.highScoreTable.name[5] = "Kyle";
	gameState->mainMenu.highScoreTable.name[6] = "Ally";
	gameState->mainMenu.highScoreTable.name[7] = "Mike";
	gameState->mainMenu.highScoreTable.name[8] = "Poopoo";
	gameState->mainMenu.highScoreTable.name[9] = "Jonny";
	gameState->mainMenu.highScoreTable.name[10] = "Thingy";

	gameState->mainMenu.highScoreTable.score[0] = 40;
	gameState->mainMenu.highScoreTable.score[1] = 80;
	gameState->mainMenu.highScoreTable.score[2] = 50;
	gameState->mainMenu.highScoreTable.score[3] = 90;
	gameState->mainMenu.highScoreTable.score[4] = 70;
	gameState->mainMenu.highScoreTable.score[5] = 100;
	gameState->mainMenu.highScoreTable.score[6] = 30;
	gameState->mainMenu.highScoreTable.score[7] = 60;
	gameState->mainMenu.highScoreTable.score[8] = 2;
	gameState->mainMenu.highScoreTable.score[9] = 9001;
	gameState->mainMenu.highScoreTable.score[10] = 0;

	BubblesortHighScore(gameState->mainMenu.highScoreTable);


				   
	gameState->timer = 0.0f;
	gameState->isEnterNameMessageShowing = false;
	gameState->currentName = "";;
}

inline glm::vec3
moveForward(glm::vec3 pos, GLfloat angle, GLfloat d)
{
	glm::vec3 result = glm::vec3(pos.x + d*std::sin(angle*Deg2Rad32), pos.y, pos.z - d*std::cos(angle*Deg2Rad32));
	return result;
}

inline glm::vec3
moveRight(glm::vec3 pos, GLfloat angle, GLfloat d)
{
	glm::vec3 result = glm::vec3(pos.x + d*std::cos(angle*Deg2Rad32), pos.y, pos.z + d*std::sin(angle*Deg2Rad32));
	return result;
}

static bool
checkAABB(AABB one, AABB other)
{
	if (one.position.x - one.size.x/2 <= other.position.x + other.size.x/2 &&
		one.position.y - one.size.y / 2 <= other.position.y + other.size.y / 2 &&
		one.position.z - one.size.z / 2 <= other.position.z + other.size.z / 2 &&
		one.position.x + one.size.x/2 >= other.position.x - other.size.x/2 &&
		one.position.y + one.size.y / 2 >= other.position.y - other.size.y / 2 &&
		one.position.z + one.size.z / 2 >= other.position.z - other.size.z / 2)
		return true;

	return false;
}


#if 0
inline void
printVec3(glm::vec3 vector)
{
	OutputLn(vector.x << ' ' << vector.y << ' ' << vector.z);
}
#endif

inline float
distance(float x_1, float x_2,
         float y_1, float y_2)
{
	float result = sqrt(Square(x_1-x_2) + Square(y_1-y_2));
	return result;
}



inline void
limitNumberToThreshhold(float &number, float threshhold)
{
	if (number > threshhold)
	{
		number = threshhold;
	}
	else if (number < -threshhold)
	{
		number = -threshhold;
	}
}

// TODO(Jonny): There is a *mathimatically correct* way of doing this; using Delta, and
//				angles, and stuff. Just cound't be bothered doing it...
static glm::vec3
moveTowardsPoint(glm::vec3 current, glm::vec3 target, float speed)
{
	glm::vec3 result = current;
	
	result.x += speed * sin(Deg2Rad32 * ((atan2(current.z-target.z, current.x-target.x) * 180.0f / Pi32) + 270.0f));
	result.z += speed * cos(Deg2Rad32 * ((atan2(current.z-target.z, current.x-target.x) * 180.0f / Pi32) + 90.0f));

	return result;
}



static void
DoHighScoreThingsAndStuff(GameState *gameState, Input input)
{
	if (input.KeyDown[A])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "A");
	}
	if (input.KeyDown[B])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "B");
	}
	if (input.KeyDown[C])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "C");
	}
	if (input.KeyDown[D])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "D");
	}
	if (input.KeyDown[E])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "E");
	}
	if (input.KeyDown[F])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "F");
	}
	if (input.KeyDown[G])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "G");
	}
	if (input.KeyDown[H])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "H");
	}
	if (input.KeyDown[I])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "I");
	}
	if (input.KeyDown[J])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "J");
	}
	if (input.KeyDown[K])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "K");
	}
	if (input.KeyDown[L])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "L");
	}
	if (input.KeyDown[M])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "M");
	}
	if (input.KeyDown[N])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "N");
	}
	if (input.KeyDown[O])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "O");
	}
	if (input.KeyDown[P])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "P");
	}
	if (input.KeyDown[Q])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "Q");
	}
	if (input.KeyDown[R])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "R");
	}
	if (input.KeyDown[S])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "S");
	}
	if (input.KeyDown[T])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "T");
	}
	if (input.KeyDown[U])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "U");
	}
	if (input.KeyDown[V])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "V");
	}
	if (input.KeyDown[W])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "W");
	}
	if (input.KeyDown[X])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "X");
	}
	if (input.KeyDown[Y])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "Y");
	}
	if (input.KeyDown[Z])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, "Z");
	}
	if (input.KeyDown[SPACE])
	{
		gameState->currentName = ConcatonateStrings(gameState->currentName, " ");
	}

	// NOTE(Jonny): I'm using LEFT instead of DELETE
	if ((input.KeyDown[DEL]) || (input.KeyDown[BACKSPACE]))
	{
		uint32_t length = StringLength(gameState->currentName);
		if (length > 0)
		{
			gameState->currentName[length-1] = '\0';
		}
	}
}

void
update(GameState *gameState, Input input, float delta)
{
	if (gameState->mainMenu.isActive)
	{
		if (input.KeyDown[ESCAPE])
		{
			if (gameState->mainMenu.isHelpShown)
			{
				gameState->mainMenu.isHelpShown = false;
			}
			else if (gameState->mainMenu.isHighScoreShowing) 
			{
				gameState->mainMenu.isHighScoreShowing = false;
			}
		}

		if (input.KeyDown[UP] || input.KeyDown[W])
		{
			gameState->mainMenu.currentlySelectedItem--;
		}
		else if (input.KeyDown[DOWN] || input.KeyDown[S])
		{
			gameState->mainMenu.currentlySelectedItem++;
		}

		if (gameState->mainMenu.currentlySelectedItem < 0)
		{
			gameState->mainMenu.currentlySelectedItem = NumberOfMainMenuItems;
		}
		else if (gameState->mainMenu.currentlySelectedItem > NumberOfMainMenuItems)
		{
			gameState->mainMenu.currentlySelectedItem = 0;
		}
		if (input.Key[SPACE])// || input.Key[RETURN])
		{
			switch(gameState->mainMenu.currentlySelectedItem)
			{
				case 0:
				{
					gameState->mainMenu.isActive = false;
				} break;

				case 1:
				{
					gameState->mainMenu.isHighScoreShowing = true;
				} break;
				case 2:
				{
					gameState->mainMenu.isHelpShown = true;
				} break;
				case 3:
				{
					exit(1);
				} break;
			}
		}
	}
	else
	{ 
		if (input.KeyDown[ESCAPE])
		{
			gameState->mainMenu.isActive = true;
			return;
		}

		if (!gameState->isEnterNameMessageShowing)
		{
			gameState->timer += delta;

			float movement = 100.0f * delta;

			if (input.Key[E])
			{
				movement *= 2.0f;
			}

			if (input.Key[W])
			{
				gameState->camera.pos = moveForward(gameState->camera.pos,
													gameState->camera.yaw,
													-movement);
			}
			if (input.Key[S])
			{
				gameState->camera.pos = moveForward(gameState->camera.pos,
													gameState->camera.yaw,
													movement);
			}

			if (input.Key[A])
			{
				gameState->camera.pos = moveRight(gameState->camera.pos,
												  gameState->camera.yaw,
												  movement);
			}
			if (input.Key[D])
			{
				gameState->camera.pos = moveRight(gameState->camera.pos,
												  gameState->camera.yaw,
												  -movement);
			}


			glm::vec2 mouseDistance = input.MouseMoveDistance;

			gameState->camera.pitch += mouseDistance.y;
			if (gameState->camera.pitch < -180.0f)
				gameState->camera.pitch = -180.0f;
			if (gameState->camera.pitch > 20)
				gameState->camera.pitch = 20;

			gameState->camera.yaw += mouseDistance.x;
			if (gameState->camera.yaw >= 360) gameState->camera.yaw -= 360;
			if (gameState->camera.yaw <= -360) gameState->camera.yaw += 360;


			gameState->camera.onBuilding = false;

			uint32_t noOfBuildings = ArrayCount(gameState->building);
			ULoop(noOfBuildings)
			{
				if (gameState->building[index].isInitialized == true)
				{
					if (!checkAABB(gameState->camera.aabb, gameState->building[index].aabb_body))
					{
						if (checkAABB(gameState->camera.aabb, gameState->building[index].aabb_roof))
						{
							gameState->camera.onBuilding = true;
							gameState->camera.currentBuilding = index;
						}
					}
					else
					{
						// NOTE(Jonny): Hacky-est code you've ever seen!!
						//				Makes the player *bounce* off the side of buildings
						//				I tried using the players previousTime for this, but it
						//				made the player *stick* to the building. This is Plan B.
						if (input.Key[W])
						{
							gameState->camera.pos = moveForward(gameState->camera.pos,
																gameState->camera.yaw,
																-movement * 10.0f);
						}
						if (input.Key[S])
						{
							gameState->camera.pos = moveForward(gameState->camera.pos,
																gameState->camera.yaw,
																movement * 10.0f);
						}

						if (input.Key[A])
						{
							gameState->camera.pos = moveRight(gameState->camera.pos,
															  gameState->camera.yaw,
															  -movement * 10.0f);
						}
						if (input.Key[D])
						{
							gameState->camera.pos = moveRight(gameState->camera.pos,
															  gameState->camera.yaw,
															  movement * 10.0f);
						};
					}
				}
			}


			float gravityPull = 9.8f;
			float mass = 2.0f;
			if (gameState->camera.onBuilding == true)
			{
				if (gameState->camera.vspeed < 0.0f)
				{
					gameState->camera.vspeed = 0.0f;
				}
			}

			gameState->camera.pos.y += gameState->camera.vspeed * delta;

			gameState->camera.lookAt.y += gameState->camera.vspeed * delta;
			gameState->camera.aabb.position = gameState->camera.pos;

			gameState->camera.vspeed -= Square(gravityPull) * mass * delta;
		


			if ((gameState->camera.onBuilding) && (input.Key[SPACE]))
			{
				gameState->camera.vspeed = 150.0f;
			}
		

			if (gameState->camera.pos.y < -60.0f)
			{
				gameState->camera.pos = gameState->camera.startPos;
				gameState->camera.lookAt = gameState->camera.startPos - glm::vec3(0.0f, 0.0f, 1.0f);
				gameState->camera.aabb.position = gameState->camera.startPos;
				gameState->camera.vspeed = 0.0f;
				gameState->timer += 10.0f;
			}

			uint32_t numberOfEnemies = ArrayCount(gameState->enemy);
			UForeach(numberOfEnemies, enemyIndex)
			{
				bool isEnemyOnBuilding = false;

				gameState->enemy[enemyIndex].aabb.position = gameState->enemy[enemyIndex].pos;
			
				uint32_t numberOfBuildings = ArrayCount(gameState->building);
				UForeach(numberOfBuildings, buildingIndex)
				{
					if (gameState->building[buildingIndex].isInitialized == true)
					{
						if(!checkAABB(gameState->enemy[enemyIndex].aabb,
						   gameState->building[buildingIndex].aabb_body))
						{
							if (checkAABB(gameState->enemy[enemyIndex].aabb,
								gameState->building[buildingIndex].aabb_roof))
							{
								isEnemyOnBuilding = true;
								gameState->enemy[enemyIndex].currentBuilding = buildingIndex;
							}
						}
					}
				}

				if (isEnemyOnBuilding == true)
				{
					if (gameState->enemy[enemyIndex].vspeed < 0.0f)
					{
						gameState->enemy[enemyIndex].vspeed = 0.0f;
					}
				}

				gameState->enemy[enemyIndex].pos.y += gameState->enemy[enemyIndex].vspeed * delta;
				gameState->enemy[enemyIndex].vspeed -= Square(gravityPull) * mass * delta;

				if (gameState->enemy[enemyIndex].currentBuilding == gameState->camera.currentBuilding)
				{
					 gameState->enemy[enemyIndex].pos = moveTowardsPoint(gameState->enemy[enemyIndex].pos,
																		 gameState->camera.pos,
																		 10.0f * delta);
				}
				gameState->enemy[enemyIndex].aabb.position = gameState->enemy[enemyIndex].pos;
			}

			gameState->camera.previousPos = gameState->camera.pos;

			if (input.KeyDown[Y])
			{
				gameState->isEnterNameMessageShowing = true;
				gameState->currentName = "\0";
			}
		}
		else
		{
			DoHighScoreThingsAndStuff(gameState, input);
			if (input.KeyDown[RETURN])
			{
				BubblesortHighScore(gameState->mainMenu.highScoreTable);

				gameState->mainMenu.highScoreTable.name[NumberOfHighScores-1] = gameState->currentName;
				gameState->mainMenu.highScoreTable.score[NumberOfHighScores-1] = gameState->camera.score;

				BubblesortHighScore(gameState->mainMenu.highScoreTable);

				gameState->isEnterNameMessageShowing = false;
				gameState->mainMenu.isActive = true;
				gameState->mainMenu.isHighScoreShowing = true;
			}
		}
	}

// NOTE(Jonny): I still love this feature, but my name begins
//				with J so it's hella-annoying to have a Breakpoint
//				hit whenever I press J to enter my name
//				in the high score board...
#if 0
#if WIN32
	if (input.KeyDown[J])
	{
		DebugBreak();
	}
#endif
#endif
}

static glm::mat4
getViewMatrix(const Camera &camera)
{
	glm::vec3 cameraLookAt = glm::vec3(
		camera.pos.x + sin(glm::radians(camera.yaw)),
		camera.pos.y + cos(glm::radians(camera.pitch)),
		camera.pos.z - cos(glm::radians(camera.yaw))
		);
	return glm::lookAt(cameraLookAt, camera.pos, glm::vec3(0.0f, 1.0f, 0.0f));
}

static Vector2
GetLetterPosition(char letter);

static void
drawLetter(char letter, float x, float y, float scale, GLuint shader)
{
	float oneFrame = 1.0f / 16.0f;
	Vector2 letterPos = GetLetterPosition(letter);

	GLuint uniformTextureOffset = glGetUniformLocation(shader, "u_offset");
	setUniform(uniformTextureOffset, glm::vec2(-(oneFrame*letterPos.x),
       						 		   		   -(oneFrame*letterPos.y)));

	glm::mat4 pos = {};
	pos = glm::translate(glm::mat4(1.0f), glm::vec3(x, y, 0.0f));
	pos = glm::scale(pos, glm::vec3(scale, scale, 0.0f));

	GLuint uniformPosition = glGetUniformLocation(shader, "u_pos");
	setUniform(uniformPosition, pos);
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
}

static void
drawWord(char *text, float x, float y, float scale, GLuint shader)
{
	uint32_t wordLength = StringLength(text);

	ULoop(wordLength)
	{
		drawLetter(text[index], x+(index*scale), y, scale, shader);
	}
}

static float
GetAngleTowardsPoint(glm::vec3 pointOne, glm::vec3 pointTwo)
{
	float result = atan2((pointTwo.x-pointOne.x), (pointTwo.z-pointOne.z));

	return result;
}


#define BUTTON_SCALE_X 0.2f
#define BUTTON_SCALE_Y 0.1f

void
draw(GameState *gameState)
{
	if (gameState->mainMenu.isActive)
	{
		glEnable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glClearColor(1.0f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(gameState->shaders.hud);
		GLuint uniformTextureOffset = glGetUniformLocation(gameState->shaders.hud, "u_offset");
		GLuint uniformPosition = glGetUniformLocation(gameState->shaders.hud, "u_pos");
		glm::mat4 pos = {};
		
		glBindVertexArray(gameState->mainMenu.background_gl.mesh);
		setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));
		
		// NOTE(Jonny): Background
		{
			pos = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
			setUniform(uniformPosition, pos);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuBackground);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		// NOTE(Jonny): Title
		{	
			pos = glm::translate(glm::mat4(1.0f), glm::vec3(0.5f, 0.4f, 0.0f));
			pos = glm::scale(pos, glm::vec3(0.4f, 0.4f, 0.0f));
			setUniform(uniformPosition, pos);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuTitle);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}


		
		glBindVertexArray(gameState->mainMenu.gl.mesh);

		// NOTE(Jonny): Start game
		{		
			if (gameState->mainMenu.currentlySelectedItem == 0)
			{
				setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));
		    }
		    else
		    {
				setUniform(uniformTextureOffset, glm::vec2(0.5f, 0.0f));
		    }
			pos = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, 0.2f, 0.0f));
			pos = glm::scale(pos, glm::vec3(BUTTON_SCALE_X, BUTTON_SCALE_Y, 0.0f));
			setUniform(uniformPosition, pos);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuStart);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}
		
		// NOTE(Jonny): Highscore game
		{	
			if (gameState->mainMenu.currentlySelectedItem == 1)
			{
				setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));
		    }
		    else
		    {
				setUniform(uniformTextureOffset, glm::vec2(0.5f, 0.0f));
		    }

			pos = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.1f, 0.0f));
			pos = glm::scale(pos, glm::vec3(BUTTON_SCALE_X, BUTTON_SCALE_Y, 0.0f));
			setUniform(uniformPosition, pos);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuHighScore);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		// NOTE(Jonny): Help game
		{	
			if (gameState->mainMenu.currentlySelectedItem == 2)
			{
				setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));
		    }
		    else
		    {
				setUniform(uniformTextureOffset, glm::vec2(0.5f, 0.0f));
		    }

			pos = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.4f, 0.0f));
			pos = glm::scale(pos, glm::vec3(BUTTON_SCALE_X, BUTTON_SCALE_Y, 0.0f));
			setUniform(uniformPosition, pos);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuHelp);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		// NOTE(Jonny): Quit game
		{	
			if (gameState->mainMenu.currentlySelectedItem == 3)
			{
				setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));
		    }
		    else
		    {
				setUniform(uniformTextureOffset, glm::vec2(0.5f, 0.0f));
		    }
			pos = glm::translate(glm::mat4(1.0f), glm::vec3(-0.5f, -0.7f, 0.0f));
			pos = glm::scale(pos, glm::vec3(BUTTON_SCALE_X, BUTTON_SCALE_Y, 0.0f));
			setUniform(uniformPosition, pos);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuQuit);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		//HELP
		if (gameState->mainMenu.isHelpShown == true)
		{
			glBindVertexArray(gameState->mainMenu.background_gl.mesh);
			setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));

			pos = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
			//pos = glm::scale(pos, glm::vec3(BUTTON_SCALE_X, BUTTON_SCALE_Y, 0.0f));
			setUniform(uniformPosition, pos);

			glBindTexture(GL_TEXTURE_2D, gameState->textures.helpScreen);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
		}

		// NOTE(Jonny): High score screen
		if (gameState->mainMenu.isHighScoreShowing == true)
		{
			glBindVertexArray(gameState->mainMenu.background_gl.mesh);
			setUniform(uniformTextureOffset, glm::vec2(0.0f, 0.0f));

			pos = glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.0f));
			setUniform(uniformPosition, pos);

			glBindTexture(GL_TEXTURE_2D, gameState->textures.mainMenuBackground);
			glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

			//glUseProgram(gameState->shaders.hud);
			
			glBindTexture(GL_TEXTURE_2D, gameState->textures.textSheet);
			glBindVertexArray(gameState->mesh.hud.mesh);
			
			float yPosition = 0.0f;
			ULoop(NumberOfHighScores-1)
			{
				gameState->mainMenu.highScoreTable = BubblesortHighScore(gameState->mainMenu.highScoreTable);				


				char *beforeThingy = ConcatonateIntAndString((index+1), ". ");
				char* name = ConcatonateStrings(beforeThingy, gameState->mainMenu.highScoreTable.name[index]);
				char *outputName = ConcatonateStrings(name, ": \t");
				char *outputScore = ConcatonateStringAndInt(outputName, gameState->mainMenu.highScoreTable.score[index]);

				drawWord(outputScore, -0.7f, yPosition, 0.05f, gameState->shaders.hud);

				free(outputScore);
				free(outputName);
				free(name);
				free(beforeThingy);

				yPosition -= 0.1f;
			}
		}
	}
	else
	{
		glEnable(GL_CULL_FACE);
		glEnable(GL_DEPTH_TEST);
		glClearColor(1.0f, 0.5f, 0.5f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glCullFace(GL_BACK);

		glm::mat4 projection(1.0f);
		projection = glm::perspective(glm::radians(60.0f), 800.0f / 600.0f, 1.0f, 10000.0f);
		glm::mat4 modelMatrix(1.0f), viewMatrix(1.0f), modelViewMatrix(1.0f);




		//Draw Skybox
		viewMatrix = getViewMatrix(gameState->camera);
		glm::mat4 rotationOnlyMat = glm::mat4(glm::transpose(glm::inverse(glm::mat3(viewMatrix))));
		modelMatrix = glm::scale(modelMatrix, glm::vec3(100.0f, 100.0f, 100.0f));
		modelViewMatrix = rotationOnlyMat * modelMatrix;
		glUseProgram(gameState->shaders.skybox);
		GLuint uniformSkyboxModelViewMat = glGetUniformLocation(gameState->shaders.skybox, "modelViewMat");
		GLuint uniformSkyboxProjectionMat = glGetUniformLocation(gameState->shaders.skybox, "projectionMat");
		glDepthMask(GL_FALSE);
		glCullFace(GL_FRONT);
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_CUBE_MAP, gameState->textures.skyboxCubemap);
		setUniform(uniformSkyboxModelViewMat, modelViewMatrix);
		setUniform(uniformSkyboxProjectionMat, projection);
		drawMesh(gameState->mesh.cube);
		glCullFace(GL_BACK);
		glDepthMask(GL_TRUE);


		// NOTE(Jonny): Render Buildings and Enemies
		glUseProgram(gameState->shaders.main);
		GLuint uniformModelViewMat = glGetUniformLocation(gameState->shaders.main, "modelViewMat");
		GLuint uniformProjectionMat = glGetUniformLocation(gameState->shaders.main, "projectionMat");
		setUniform(uniformProjectionMat, projection);

		glBindTexture(GL_TEXTURE_2D, 0);
		uint32_t numberOfEnemies = ArrayCount(gameState->enemy);
		ULoop(numberOfEnemies)
		{
			if (gameState->enemy[index].isInitialized == true)
			{
				modelMatrix = glm::mat4(1.0f);
				modelMatrix = glm::translate(modelMatrix, gameState->enemy[index].pos);
				modelMatrix = glm::scale(modelMatrix, gameState->enemy[index].scale);
				float rotation = GetAngleTowardsPoint(gameState->enemy[index].pos,
				                                      gameState->camera.pos);
				modelMatrix = glm::rotate(modelMatrix,
				                          rotation,
				                          glm::vec3(0.0f, 1.0f, 0.0f));
				modelViewMatrix = viewMatrix * modelMatrix;

				setUniform(uniformModelViewMat, modelViewMatrix);
				drawMesh(gameState->mesh.enemy);
			}
		}

		glBindTexture(GL_TEXTURE_2D, gameState->textures.building);
		uint32_t buildingNo = ArrayCount(gameState->building);
		ULoop(buildingNo)
		{
			if (gameState->building[index].isInitialized == true)
			{
				modelMatrix = glm::mat4(1.0f);
				modelMatrix = glm::translate(modelMatrix, gameState->building[index].pos);
				modelMatrix = glm::rotate(modelMatrix,
										  glm::radians(gameState->building[index].rotation.x),
										  glm::vec3(1.0f, 0.0f, 0.0f));
				modelMatrix = glm::rotate(modelMatrix,
										  glm::radians(gameState->building[index].rotation.y),
										  glm::vec3(0.0f, 1.0f, 0.0f));
				modelMatrix = glm::rotate(modelMatrix,
										  glm::radians(gameState->building[index].rotation.z),
										  glm::vec3(0.0f, 0.0f, 1.0f));
				modelMatrix = glm::scale(modelMatrix, gameState->building[index].scale);
				modelViewMatrix = viewMatrix * modelMatrix;

				glUseProgram(gameState->shaders.main);
				setUniform(uniformModelViewMat, modelViewMatrix);
				drawMesh(gameState->mesh.buildings[0]);

				#if COLLISION_BOXES
					drawCollisionBox(gameState->building[index].aabb_roof, gameState->shaders.wireFrame,
						projection, viewMatrix);
					drawCollisionBox(gameState->building[index].aabb_body, gameState->shaders.wireFrame,
						projection, viewMatrix);
				#endif
			}
		}


		glUseProgram(gameState->shaders.hud);
		glBindTexture(GL_TEXTURE_2D, gameState->textures.textSheet);
		glBindVertexArray(gameState->mesh.hud.mesh);

		char *outputScore = ConcatonateStringAndInt("Score: ", gameState->camera.score);
		drawWord(outputScore, 0.7f, 0.9f, 0.03f, gameState->shaders.hud);
		free(outputScore);

		char *numberOfPizzaSlicesLeft = ConcatonateStringAndInt("Slices Left: ",
																gameState->camera.numberOfPizzaSlices);
		drawWord(numberOfPizzaSlicesLeft, -0.9f, 0.9f, 0.03f, gameState->shaders.hud);
		free(numberOfPizzaSlicesLeft);

		char *timerOutput = ConcatonateStringAndInt("Time: ", (int)gameState->timer);
		drawWord(timerOutput, -0.9f, 0.7f, 0.08f, gameState->shaders.hud);
		free(timerOutput);

		if (gameState->isEnterNameMessageShowing)
		{
			char *output = ConcatonateStrings("Enter Name: ", gameState->currentName);
			drawWord(output, -0.9f, 0.0f, 0.05f, gameState->shaders.hud);
			free(output);
		}


		#if DRAW_BOXES
			drawAABB(gameState->camera.aabb, gameState->shaders.wireFrame, projection, viewMatrix);
		#endif
	}

}