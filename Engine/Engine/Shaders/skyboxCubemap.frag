// cubeMap fragment shader
#version 330

precision highp float;

layout (location = 0) out vec4 outColour;
in vec3 cubeTexCoord;

uniform samplerCube cubeMap;
 
void main(void) 
{   
	outColour = texture(cubeMap, cubeTexCoord);
}